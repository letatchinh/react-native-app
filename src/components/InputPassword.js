import AntIcon from 'react-native-vector-icons/AntDesign';
import FeatherIcon from 'react-native-vector-icons/Feather';

import React, { useState } from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import InputBase from './FormComponents/InputBase';
import { colorPrimary } from '../styles/variable';

export default function InputPassword({ handlePasswordChange, password }) {
  const [hide,setHide] = useState(true);
  const toggle = () => {
    setHide(!hide)
  }
  return (
    <View style={styles.container}>
      <InputBase
        value={password}
        placeholder="Mật khẩu"
        onChangeText={handlePasswordChange}
        secureTextEntry={hide}
        InputLeftElement={<FeatherIcon name="lock" size={18} color={colorPrimary}/>}
      />
      <TouchableOpacity onPress={toggle} style={styles.eye}>
      {hide ? <AntIcon name="eyeo" size={24} color={colorPrimary}/>  : <FeatherIcon name="eye-off" size={24} color={colorPrimary}/>}
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
container:{
    position: 'relative',
    width: '100%'
  },
  eye:{
    position : 'absolute',
    right : 14,
    top : 16,
    bottom : '25%',
    zIndex : 2,
  }
});
