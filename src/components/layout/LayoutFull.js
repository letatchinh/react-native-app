import React from 'react';
import { ScrollView, View } from 'react-native';
import styleStore from '../../styles';
import SafeAreaViewFull from '../SafeAreaViewFull';

export default function LayoutFull({children}) {
  return (
    <SafeAreaViewFull>
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        >
        <View style={styleStore.CommonStyles.container}>
        {children}
        </View>
      </ScrollView>
    </SafeAreaViewFull>
  )
}
