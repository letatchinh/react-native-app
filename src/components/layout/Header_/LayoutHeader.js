import { Row } from 'native-base';
import React from 'react';
import styles from '../../../styles';

export default function LayoutHeader({children}) {
  return (
    <Row style={styles.HeaderStyles.row} justifyContent='space-between' alignItems={'center'}>
        {children}
    </Row>
  )
};

