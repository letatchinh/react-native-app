import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { TouchableOpacity } from 'react-native';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import IonIcon from 'react-native-vector-icons/Ionicons';
import { HOME_SCREEN } from '../../../constants/screen';
import { colorPrimary } from '../../../styles/variable';
import TextStrong from '../../TextCustom/TextStrong';
import LayoutHeader from './LayoutHeader';

export default function HeaderProfile() {
    const navigation = useNavigation(); // Access the navigation object

    const onGoBack = () => {
        navigation.replace(HOME_SCREEN);
    }
    return (
        <LayoutHeader>
            <TouchableOpacity onPress={onGoBack}>
                <EntypoIcon name='chevron-left' size={24} />
            </TouchableOpacity>
            <TextStrong style={{ fontSize: 18 }}>Tư vấn từ xa</TextStrong>
            <IonIcon name='notifications' color={colorPrimary} size={24} />
        </LayoutHeader>
    )
};

