import { useNavigation } from '@react-navigation/native';
import { Avatar } from 'native-base';
import React from 'react';
import { TouchableOpacity } from 'react-native';
import IonIcon from 'react-native-vector-icons/Ionicons';
import { PROFILE_SCREEN } from '../../../constants/screen';
import CommonStyles from '../../../styles/common';
import { colorPrimary } from '../../../styles/variable';
import TextStrong from '../../TextCustom/TextStrong';
import LayoutHeader from './LayoutHeader';

export default function HeaderHome() {
  const navigation = useNavigation(); // Access the navigation object
  const onNavigateProfile = () => {
    navigation.replace(PROFILE_SCREEN)
  }
  return (
    <LayoutHeader>
        <TouchableOpacity onPress={onNavigateProfile}>
        <Avatar style={CommonStyles.wrapCircles} bg="green.500" size={'sm'} source={{
      uri: "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80"
    }}/>
        </TouchableOpacity>
        <TextStrong style={{fontSize : 18}}>Tư vấn từ xa</TextStrong>
        <IonIcon name='notifications' color={colorPrimary} size={24}/>
    </LayoutHeader>
  )
};

