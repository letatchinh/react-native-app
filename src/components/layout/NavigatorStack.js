import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React from 'react';
import allScreen from '../../screens/allScreen';
import WaitingScreen from '../../screens/WaitingScreen';
import LayoutFull from './LayoutFull';

const Stack = createNativeStackNavigator();
const Wrapper = ({ Component, navigation }) => {
  return <LayoutFull>
    <Component navigation={navigation} />
  </LayoutFull>
}
export default function NavigatorStack() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ animation: 'none' }}>
        <Stack.Screen
          key={'WAITING'}
          name={'WAITING'}
          component={WaitingScreen}
          options={{title : ''}}
        />
        {allScreen?.map(({ name, component: Component, options = {} }) => (
          <Stack.Screen
            key={name}
            name={name}
            options={options}
          >
            {(props) => <Wrapper navigation={props.navigation} Component={Component} />}
          </Stack.Screen>
        ))}
      </Stack.Navigator>
    </NavigationContainer>
  );
}