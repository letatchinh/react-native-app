import { Row } from 'native-base';
import React from 'react'
import { ActivityIndicator, StyleSheet, Text, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient'
import stylesCommon from '../../styles/common';

export default function ButtonLinearGradientPrimary({ children, style, onPress, disabled = false, loading = false }) {
    return (
        <TouchableOpacity disabled={disabled || loading} onPress={onPress} style={{ width: '100%' }}>
            <LinearGradient
                colors={disabled ? ['#D4D4D8', '#D4D4D8'] : ['#3481ff', '#103393']} style={{ ...styles.linearGradient, ...style }}>
                <Row>
                    <Text style={styles.text}>
                        {loading && <ActivityIndicator size="small" color="#D4D4D8" />}
                    </Text>
                    <Text style={styles.text}>
                        {children}
                    </Text>
                </Row>
            </LinearGradient>
        </TouchableOpacity>
    )
}

var styles = StyleSheet.create({
    linearGradient: {
        flex: 1,
        paddingHorizontal: 20,
        paddingVertical: 15,
        borderRadius: 32,
        width: '100%',
        ...stylesCommon.flexCenter
    },
    text: {
        color: 'white',
        fontSize: 14,
        fontWeight: 'bold',
    },
    buttonText: {
        fontSize: 18,
        fontFamily: 'Gill Sans',
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        backgroundColor: 'transparent',
    },
});