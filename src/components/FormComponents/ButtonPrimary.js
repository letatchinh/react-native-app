import { Button } from 'native-base'
import React from 'react'

export default function ButtonPrimary({children,onPress,style,...props}) {
  return (
    <Button style={style} {...props} colorScheme="blue">{children}</Button>
  )
}
