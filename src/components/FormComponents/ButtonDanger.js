import { Button } from 'native-base'
import React from 'react'
import { StyleSheet } from 'react-native'
import CommonStyles from '../../styles/common'

export default function ButtonDanger({children,onPress,style,...props}) {
  return (
    <Button onPress={onPress} style={{...styles.button,...style}} {...props} colorScheme="red">{children}</Button>
  )
}

const styles = StyleSheet.create({
    button:{
        ...CommonStyles.flexCenter,
        width : '100%',
    },  
})
