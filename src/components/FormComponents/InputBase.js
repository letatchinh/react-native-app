import { Input, Stack } from 'native-base'
import { TextInput, View } from 'react-native'
import styleStore from '../../styles'

export default function InputBase({value,style,placeholder,onChangeText,secureTextEntry=false,...props}) {
  return (
    <Stack style={{...styleStore.InputStyles.input,...styleStore.InputStyles.bgPrimary,...style}} alignItems={'center'}>
      <Input
      style={{fontSize : 14}}
        value={value}
        placeholder={placeholder}
        onChangeText={onChangeText}
        type={secureTextEntry ? 'password' : 'text'}
        placeholderTextColor='#79A8FF'
        variant={'unstyled'}
        {...props}
      />
    </Stack>
  )
}
