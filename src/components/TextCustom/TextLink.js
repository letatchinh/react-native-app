import React from 'react'
import { Text } from 'react-native'
import styleStore from '../../styles'

export default function TextLink({children,style}) {
  return (
    <Text style={{...styleStore.TextStyles.primary,...styleStore.TextStyles.link,...styleStore.TextStyles.fontSize_14,...style}}>{children}</Text>
  )
}
