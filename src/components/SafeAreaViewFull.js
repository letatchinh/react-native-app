import React from 'react'
import { SafeAreaView } from 'react-native-safe-area-context'

export default function SafeAreaViewFull({children}) {
  return (
    <SafeAreaView edges={['left','right','bottom']} style={{backgroundColor:'white',flex:1}}>
        {children}
    </SafeAreaView>
  )
}
