import {StyleSheet} from 'react-native';

const HeaderStyles = StyleSheet.create({
    row : {
        flex : 1,
        width : '100%',
        paddingLeft : 20,
        paddingRight : 20,
    }
  });

  export default HeaderStyles