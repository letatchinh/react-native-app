export const colorPrimary = '#3983FF'; // blue
export const bgColorPrimary = '#3983FF'; // blue
export const colorPrimaryBold = '#2E2C80';
export const borderRadiusPrimary = 32;