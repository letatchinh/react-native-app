import {StyleSheet} from 'react-native';
import { colorPrimary, colorPrimaryBold } from './variable';

const TextStyles = StyleSheet.create({
    strong: {
        fontWeight: 'bold',
    },
    link: {
        color : '#5b73e8'
    },
    primaryBold:{
        color : colorPrimaryBold
    },
    primary:{
        color : colorPrimary
    },
    fontSize_14:{
        fontSize : 14,
    },
    fontSize_18:{
        fontSize : 18,
    },
    fontSize_24:{
        fontSize : 24,
    },
    fontSize_32:{
        fontSize : 32,
    },
  });

  export default TextStyles