import CommonStyles from "./common";
import InputStyles from "./input";
import TextStyles from "./text";
import HeaderStyles from "./header";

export default {
    CommonStyles,
    InputStyles,
    TextStyles,
    HeaderStyles,
}