import {StyleSheet} from 'react-native';

const InputStyles = StyleSheet.create({
    input: {
      width: '100%',
      marginBottom: 10,
      paddingLeft: 10,
      paddingVertical : 10,
      borderWidth: 0,
      backgroundColor: 'white',
      borderRadius: 8,
    },
    bgPrimary:{
        backgroundColor: '#F2F7FF',
        borderColor:'#79A8FF',
        borderWidth : 1,
        // color:'#79A8FF',
    }
  });

  export default InputStyles