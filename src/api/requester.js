import axios from 'axios';
import { API_URL } from '../constants/defaultValue';
import { getTokenFromAsyncStore } from '../hook/auth';
const responseBody = (res) => res.data;

export const getBaseURL = () => {
  return typeof window === 'undefined'
    ? process.env.API_URL || API_URL
    : API_URL;
};
export default {
  getOutJson: async (url, locale = 'vi') => {
    const response = await fetch(`${API_URL}${url}`, {
      headers: {
        'Accept-Language': locale,
      },
    });
    return await response.json();
  },

  get: async (url, locale = 'vi') =>
    fetch(`${getBaseURL()}${url}`, {
      headers: {
        'Accept-Language': locale,
      },
    }),

  post: (url, data = {}, locale = 'vi') =>
    axios.post(`${getBaseURL()}${url}`, data, {
      headers: {
        'Accept-Language': locale,
        'Content-Type': 'application/json',
      },
    }).then(responseBody)
  ,

  put: (url, data = {}, locale = 'vi') =>
    axios.put(`${getBaseURL()}${url}`, data, {
      headers: {
        'Accept-Language': locale,
        'Content-Type': 'application/json',
      },
    }).then(responseBody)
  ,
  postFormData: async (url, data) => {
    axios.post(`${getBaseURL()}${url}`, data).then(responseBody)
  },
  getWithToken: async (url, locale = 'vi', params) => {
    const accessToken = await getTokenFromAsyncStore();
    return axios.get(`${getBaseURL()}${url}`, params, {
      headers: {
        'Accept-Language': locale,
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken || ''}`
      },
    }).then(responseBody)
  },
  postWithToken: async (url, data = {}, locale = 'vi') => {
    const accessToken = await getTokenFromAsyncStore();
    return axios.get(`${getBaseURL()}${url}`, data, {
      headers: {
        'Accept-Language': locale,
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken || ''}`
      },
    }).then(responseBody)
  },
  putWithToken: async (url, data = {}, locale = 'vi') => {
    const accessToken = await getTokenFromAsyncStore();
    return axios.put(`${getBaseURL()}${url}`, data, {
      headers: {
        'Accept-Language': locale,
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken || ''}`
      },
    }).then(responseBody)
  },
  deleteWithToken: async (url, data = {}, locale = 'vi') => {
    const accessToken = await getTokenFromAsyncStore();
    return axios.delete(`${getBaseURL()}${url}`, data, {
      headers: {
        'Accept-Language': locale,
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken || ''}`
      },
    }).then(responseBody)
  },
};
