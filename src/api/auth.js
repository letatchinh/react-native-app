import { get, omit } from 'lodash';
import requester from './requester';

export default {
  postSignIn: data => requester.post('/api/v1/authenticate', data),
  postSignInWithPhone: data => requester.post('/api/v1/auth/sms-login', data),
  postSignUp: data => requester.post('/api/v1/register', data),
  postResetPassword: data => requester.put('/api/v1/customer-account/reset-password', data),
  checkPhone: data => requester.post('/api/v1/customer-account/check-phone', data),
  getProfile: () => requester.getWithToken('/api/v1/profile'),
  postProfile: data => requester.putWithToken('/api/v1/profile', data),
  getOrders: (locale, params) =>
    requester.getWithToken('/api/v1/me/order', locale, params),
  findOrderByNumber: id => requester.getWithToken(`/api/v1/me/order/${id}`),
  cancelOrder: (id, data) =>
    requester.putWithToken(`/api/v1/me/order/${id}/cancel`, data),
  
  //user service
  getBills: (locale, params) => requester.getWithToken(`/api/v1/me/wh-bill`,locale, params),
  getBillById: (id, locale) => requester.getWithToken(`/api/v1/me/wh-bill/${id}`, locale),
  getBillItemById: (billId, billItemId, locale) => requester.getWithToken(`/api/v1/me/wh-bill/${billId}/wh-bill-item/${billItemId}`, locale),
  cancelBill: (id, data={}, locale='vi') => requester.putWithToken(`/api/v1/me/wh-bill/${id}/cancel`, data, locale),
  getSessionOfDayById: (id, locale) => requester.getWithToken(`/api/v1/wh-session-of-day/${id}`, locale),
  putRatingBillItem: (billId, billItemId, data={}, locale='vi') => requester.putWithToken(`/api/v1/me/wh-bill/${billId}/wh-bill-item/${billItemId}/rate`, data, locale),
  checkActiveWithPhone: (phone, locale, params) => requester.post(`/api/v1/auth/sms-login/check/${phone}`,),
  //appointments
  getAppointments: (locale, params) => requester.getWithToken('/api/v1/me/wh-appointment', locale, params),
  getPartnerById: (id, locale, params) => requester.getWithToken(`/api/v1/wh-partner?partnerId=${id}`),
  refreshLink: id => requester.putWithToken(`/api/v1/customer-account/${id}/refresh`),
  getRefs: query => requester.getWithToken(`/api/v1/customer-account-parent-child/${get(query,'id','')}`,omit(query,['id'])),

};
