export const formatPhone = (phone) => phone.replace(/^(\+84)/, "0");// format +84 with 0
export const formatPhoneV2 = (phone) => phone.replace(/^0/, "+84");// format 0 with +84
