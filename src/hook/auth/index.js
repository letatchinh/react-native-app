import { get } from 'lodash';
import { useSelector } from "react-redux";
import { actions } from "../../redux/auth/reducer";
import { getAsyncStore, useFailed, useFetchByParam, useSubmit, useSuccess } from "../utils";

const getSelector = (key) => (state) => state.auth[key];
const tokenSelector = getSelector('token');
const isLoadingSelector = getSelector('isLoading');
const profileSelector = getSelector('profile');
const loginFailedSelector = getSelector('loginFailed');
const getProfileFailedSelector = getSelector('getProfileFailed');
const isGetProfileLoadingFailedSelector = getSelector('isGetProfileLoading');
export const useLogin = (callback) => {
    useSuccess(tokenSelector,"Đăng nhập thành công",callback);
    useFailed(loginFailedSelector,"Đăng nhập thất bại");
    return useSubmit({
        loadingSelector : isLoadingSelector,
        action : actions.loginRequest,
        
    })
};
export const useLogout = () => {
    return useSubmit({
        loadingSelector : isLoadingSelector,
        action : actions.logoutRequest,
    })
};

export const useProfile = () => {
    const profile = useSelector(profileSelector);
    return profile
};
export const useToken = () => {
    const token = useSelector(tokenSelector);
    return token
};


export const useGetProfile = () => {
    const token = useSelector(tokenSelector);
    return useFetchByParam({
        action : actions.getProfileRequest,
        dataSelector : profileSelector,
        failedSelector : getProfileFailedSelector,
        loadingSelector : isGetProfileLoadingFailedSelector,
        param : token,
    })
}

export const getTokenFromAsyncStore = async() => {
    const persist = await getAsyncStore("persist:auth");
    return get(persist,'token');
}