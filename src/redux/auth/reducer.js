import { createSlice } from '@reduxjs/toolkit'
import { clearAsyncStorage } from '../../hook/utils';

const initialState = {
    isLoading: false,

    token: null,
    loginFailed: null,

    profile: null,
    isGetProfileLoading: false,
    getProfileFailed: null,

}

export const auth = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        // LOGIN
        loginRequest: (state, { payload }) => {
            state.isLoading = true;
            state.token = null;
            state.loginFailed = null;
        },
        loginSuccess: (state, { payload }) => {
            state.token = payload.token;
        },
        loginFailed: (state, { payload }) => {
            state.loginFailed = payload;
            state.isLoading = false;
        },
        logoutRequest: () => {
            // removeAxiosToken();
            clearAsyncStorage(); // FIX ME: use await request if failed
            return initialState
        },

        // GET PROFILE

        getProfileRequest: (state, { payload }) => {
            state.isGetProfileLoading = true;
            state.getProfileFailed = null;
        },
        getProfileSuccess: (state, { payload }) => {
            state.isGetProfileLoading = false;
            state.profile = payload;
        },
        getProfileFailed: (state, { payload }) => {
            state.isGetProfileLoading = false;
            state.getProfileFailed = payload;
        },
    },
})

// Action creators are generated for each case reducer function
export const actions = auth.actions

export default auth.reducer