import { put, call, takeLatest } from 'redux-saga/effects';
import api from '../../api';
import { actions } from './reducer';

function* login({ payload: user }) {
  try {
    const {token,userId} = yield call(api.auth.postSignIn, user);
    yield put(actions.loginSuccess({token,userId}));
  } catch (error) {
    yield put(actions.loginFailed(error));
  }
}


function* getProfile({ payload: token }) {
  try {
    const profile = yield call(api.auth.getProfile, token);
    yield put(actions.getProfileSuccess(profile));
  } catch (error) {
    yield put(actions.getProfileFailed(error));
  }
}


export default function* userSaga() {
  yield takeLatest(actions.loginRequest, login);
  yield takeLatest(actions.getProfileRequest, getProfile);
}
