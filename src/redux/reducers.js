import { persistReducer } from 'redux-persist';

import { combineReducers } from "redux";

import AsyncStorage from '@react-native-async-storage/async-storage';
import auth from './auth/reducer';
const authPersistConfig = {
    key: 'auth',
    storage: AsyncStorage,
    blacklist: [
        'loginFailed',
        'isLoading',
        'isGetProfileLoading',
        'getProfileFailed'
      ]
  };
const rootReducer = combineReducers({
    auth: persistReducer(authPersistConfig, auth),
});
export default rootReducer