import React, { useEffect } from 'react';
import { Image, View } from 'react-native';
import { HOME_SCREEN, WELCOME_SCREEN } from '../constants/screen';
import { useToken } from '../hook/auth';
import styleStore from '../styles';

export default function WaitingScreen({ navigation }) {
  const token = useToken();
  useEffect(() => {
      setTimeout(() => {
        if(token){
          navigation.replace(HOME_SCREEN);
        }else{
          navigation.replace(WELCOME_SCREEN);
        }
      }, 700);
  }, [token]);
  return (
    <View style={{ ...styleStore.CommonStyles.container, ...styleStore.CommonStyles.bg_white }}>
      <Image source={require('../assets/images/checking-order.png')} style={{ width: 200, height: 200 }} resizeMode='contain' />
    </View>
  )
}
