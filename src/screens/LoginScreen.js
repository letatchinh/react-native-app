import { useNavigation } from '@react-navigation/native';
import { Checkbox } from 'native-base';
import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import AntIcon from 'react-native-vector-icons/AntDesign';
import LogoWC from '../assets/images/logoWC.svg';
import ButtonLinearGradientPrimary from '../components/FormComponents/ButtonLinearGradientPrimary';
import InputBase from '../components/FormComponents/InputBase';
import InputPassword from '../components/InputPassword';
import TextLink from '../components/TextCustom/TextLink';
import { HOME_SCREEN, REGISTER_SCREEN } from '../constants/screen';
import { useLogin } from '../hook/auth';
import styleStore from '../styles';
import { colorPrimary } from '../styles/variable';
import { formatPhoneV2 } from '../utils/helper';

const LoginScreen = () => {
  const navigation = useNavigation(); // Access the navigation object

  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');
  const [agreePolicy,setAgreePolicy] = useState(false);
  const [isLoading,onLogin] = useLogin(
    () => {
      navigation.replace(HOME_SCREEN);
      navigation.reset({
        index: 1,
        routes: [{ name: HOME_SCREEN }],       
   })
    }
    );
  const handleUsernameChange = text => {
    setLogin(text);
  };

  const handlePasswordChange = text => {
    setPassword(text);
  };
  const handleAgreeChange = value => {
    setAgreePolicy(value);
  }
  const handleLogin = () => {
    const formatLogin = formatPhoneV2(login);
    onLogin({login:formatLogin, password})
  };
  const onNavigateRegister = () => {
    navigation.navigate(REGISTER_SCREEN)
  }
  return (


    <View style={{ ...styleStore.CommonStyles.gap_10, ...styleStore.CommonStyles.container, ...styleStore.CommonStyles.padding_20 }}>
      <LogoWC width={200} height={200} />
      <InputBase
        InputLeftElement={<AntIcon name="user" size={18} color={colorPrimary} />}
        value={login}
        placeholder={"Số điện thoai/Email"}
        onChangeText={handleUsernameChange}
      />
      <InputPassword
        password={password}
        handlePasswordChange={handlePasswordChange}
      />

      <Checkbox value={agreePolicy} onChange={handleAgreeChange} borderColor={colorPrimary} ><Text style={styles.agreePolicy}>Tôi đồng ý với <TextLink style={{ fontSize: 12 }}>Chính sách và điều khoản dịch vụ</TextLink></Text></Checkbox>
      <ButtonLinearGradientPrimary loading={isLoading} onPress={handleLogin} disabled={!agreePolicy}>
        Đăng nhập
      </ButtonLinearGradientPrimary>
      <Text style={{ marginTop: 20 }}>Bạn chưa có tài khoản ? <Text onPress={onNavigateRegister} style={{ ...styleStore.TextStyles.link, ...styleStore.TextStyles.strong }}>Đăng ký</Text></Text>
    </View>
  );
};

const styles = StyleSheet.create({
  agreePolicy: {
    fontWeight: '500',
    fontSize: 12,
  }
})

export default LoginScreen;
