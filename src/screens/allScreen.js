import HeaderHome from "../components/layout/Header_/HeaderHome"
import HeaderProfile from "../components/layout/Header_/HeaderProfile"
import * as Screens from "../constants/screen"
import HomeScreen from "./HomeScreen"
import LoginScreen from "./LoginScreen"
import ProfileScreen from "./ProfileScreen"
import RegisterScreen from "./RegisterScreen"
import WelcomeScreen from "./WelcomeScreen"

const allScreen = [
    {
        name: Screens.LOGIN_SCREEN,
        component: LoginScreen,
        options: { title: 'Đăng nhập' },
    },
    {
        name: Screens.REGISTER_SCREEN,
        component: RegisterScreen,
        options: { title: 'Đăng ký' },
    },
    {
        name: Screens.WELCOME_SCREEN,
        component: WelcomeScreen,
        options: { title: 'Chào mừng' },
    },
    {
        name: Screens.HOME_SCREEN,
        component: HomeScreen,
        options: {
            headerTitle: () => <HeaderHome/>,
            headerLeft: null // Remove the back button
        },
    },
    {
        name: Screens.PROFILE_SCREEN,
        component: ProfileScreen,
        options: {
            headerTitle: () => <HeaderProfile />,
            headerLeft: null // Remove the back button
        },
    },
]
export default allScreen