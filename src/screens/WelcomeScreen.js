import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import styleStore from '../styles'
import LogoWC from '../assets/images/logoWC.svg';
import TextLink from '../components/TextCustom/TextLink';
import TextStrong from '../components/TextCustom/TextStrong';
import ButtonLinearGradientPrimary from '../components/FormComponents/ButtonLinearGradientPrimary';
import { Button } from 'native-base';
import { LOGIN_SCREEN, REGISTER_SCREEN } from '../constants/screen';

export default function WelcomeScreen({ navigation }) {


    const onNavigateRegister = () => {
        navigation.navigate(REGISTER_SCREEN)
    };
    const onNavigateLogin = () => {
        navigation.navigate(LOGIN_SCREEN)
    }
    return (
        <View style={{ ...styleStore.CommonStyles.container, ...styleStore.CommonStyles.bg_white, ...styleStore.CommonStyles.padding_50 }}>
            <LogoWC width={200} height={200} />

            <TextStrong style={{ textAlign: 'center', fontSize: 13 }}>Chào mừng bạn đến với WorldCareVN</TextStrong>

            <TextLink style={styleStore.CommonStyles.marginTop_5}>Bạn đã có tài khoản chưa?</TextLink>

            <ButtonLinearGradientPrimary onPress={onNavigateLogin} style={styleStore.CommonStyles.marginTop_30}>Đăng nhập</ButtonLinearGradientPrimary>

            <Button onPress={onNavigateRegister} style={style.btn_register} borderColor={'blue.400'} colorScheme={'blue'} size="sm" variant="outline">
                Đăng ký
            </Button>
        </View>
    )
}


const style = StyleSheet.create({
    btn_register: {
        ...styleStore.CommonStyles.borderRadius_Primary,
        width: '100%',
        marginTop: 20,
        paddingTop: 15,
        paddingBottom: 15,
    }
});