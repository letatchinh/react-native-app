import { useNavigation } from '@react-navigation/native';
import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import ButtonDanger from '../components/FormComponents/ButtonDanger'
import { WELCOME_SCREEN } from '../constants/screen';
import { useLogout } from '../hook/auth';
import CommonStyles from '../styles/common'
export default function ProfileScreen() {
    const navigation = useNavigation(); // Access the navigation object
    const [,onLogout] = useLogout();
    const handleLogout = () => {
        onLogout();
        navigation.replace(WELCOME_SCREEN)
    };
  return (
    <View style={CommonStyles.container}>
        <ButtonDanger onPress={handleLogout}>
            <Text style={styles.textButton}>
                Đăng xuất
            </Text>
        </ButtonDanger>
    </View>
  )
}

const styles = StyleSheet.create({
    textButton:{
        color : 'white',
        fontWeight : 700,
        fontSize : 18
    },
})