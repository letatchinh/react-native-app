import React, { useState } from 'react';
import { Text, View } from 'react-native';
import LogoWC from '../assets/images/logoWC.svg';
import ButtonLinearGradientPrimary from '../components/FormComponents/ButtonLinearGradientPrimary';
import InputBase from '../components/FormComponents/InputBase';
import InputPassword from '../components/InputPassword';
import { LOGIN_SCREEN } from '../constants/screen';
import styleStore from '../styles';
import AntIcon from 'react-native-vector-icons/AntDesign';
import { colorPrimary } from '../styles/variable';

const RegisterScreen = ({navigation}) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleUsernameChange = text => {
    setUsername(text);
  };

  const handlePasswordChange = text => {
    setPassword(text);
  };

  const handleRegister = () => {
    // You can implement your authentication logic here
    // For now, let's just log the username and password
    console.log('Username:', username);
    console.log('Password:', password);
  };
  const onNavigateLogin = () => {
    navigation.navigate(LOGIN_SCREEN)
  }
  return (
    <View style={{...styleStore.CommonStyles.container,...styleStore.CommonStyles.gap_10,...styleStore.CommonStyles.padding_20}}>
     <LogoWC width={200} height={200}/>
     <InputBase
        InputLeftElement={<AntIcon name="user" size={18} color={colorPrimary}/>}
        value={username}
        placeholder={"Tên"}
        onChangeText={handleUsernameChange}
      />
     <InputBase
        InputLeftElement={<AntIcon name="mail" size={18} color={colorPrimary}/>}
        value={username}
        placeholder={"Số điện thoai/Email"}
        onChangeText={handleUsernameChange}
      />
      <InputPassword
        password={password}
        handlePasswordChange={handlePasswordChange}
      />
      <ButtonLinearGradientPrimary>
      Đăng ký
      </ButtonLinearGradientPrimary>
      <Text>Bạn đã có tài khoản ? <Text onPress={onNavigateLogin} style={{...styleStore.TextStyles.link,...styleStore.TextStyles.strong}}>Đăng nhập</Text></Text>
    </View>
  );
};


export default RegisterScreen;
