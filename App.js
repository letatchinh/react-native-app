import { NativeBaseProvider } from 'native-base';
import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider } from 'react-redux';
import NavigatorStack from './src/components/layout/NavigatorStack';
import { store } from './src/redux/store';

function App() {
  return (
    <SafeAreaProvider>
      <Provider store={store}>
        <NativeBaseProvider>
          <NavigatorStack />
        </NativeBaseProvider>
      </Provider>
    </SafeAreaProvider>
  );
}

export default App;
